//
//  SettingsTableViewController.swift
//  Open Adblock
//
//  Created by Denis Zamataev on 2015-10-15.
//  Copyright © 2015 MetroAdBlock. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController, UIPopoverPresentationControllerDelegate {
    @IBOutlet weak var explanationImageView: UIImageView! {
        didSet {
            explanationImageView.image = UIImage(named: NSLocalizedString("explanation_image_name", comment: ""));
        }
    }
    
    let testURLString = "https://dzamataev.github.io/gh-metroblockads/testpageON.html";
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    @IBAction func openSettings(_ sender: AnyObject) {
        if let settingsURL = URL(string: UIApplicationOpenSettingsURLString) {
            if (UIApplication.shared.canOpenURL(settingsURL)) {
                UIApplication.shared.openURL(settingsURL)
            }
        }
    }
    
    @IBAction func openTestPage(_ sender: AnyObject) {
        if let settingsURL = URL(string: testURLString) {
            UIApplication.shared.openURL(settingsURL)
        }
    }
    
    @IBAction func share(_ sender: AnyObject) {
        let textToShare = NSLocalizedString("sharing_message", comment: "")
        
        if let myWebsite = URL(string: "https://www.facebook.com/metroblockads")
        {
            let objectsToShare = [textToShare, myWebsite] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            switch UIDevice.current.userInterfaceIdiom {
            case .phone:
                self.present(activityVC, animated: true, completion: nil)
            case .pad:
                activityVC.modalPresentationStyle = UIModalPresentationStyle.popover
                let popover = activityVC.popoverPresentationController as UIPopoverPresentationController!
                popover?.sourceView = sender as? UIView;
                
                self.present(activityVC, animated: true, completion: nil)
            case .unspecified: break
            default: break
            }
        }
    }
}
