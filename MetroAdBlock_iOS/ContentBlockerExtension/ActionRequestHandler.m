//
//  ActionRequestHandler.m
//  MetroAdBlock
//
//  Created by Denis on 9/27/15.
//  Copyright © 2015 OpenAdblock. All rights reserved.
//

#import "ActionRequestHandler.h"

@implementation ActionRequestHandler

- (void)beginRequestWithExtensionContext:(NSExtensionContext *)context {
    NSArray *returningItems;
    
    NSURL *resourceURL = [[NSBundle mainBundle] URLForResource:@"metroBlockerList" withExtension:@"json"];
    
    NSItemProvider *attachment = [[NSItemProvider alloc] initWithContentsOfURL:resourceURL];
    
    NSExtensionItem *item;
    item = [[NSExtensionItem alloc] init];
    item.attachments = @[attachment];
    
    returningItems = @[item];
    
    
    [context completeRequestReturningItems:returningItems completionHandler:nil];
}

@end
