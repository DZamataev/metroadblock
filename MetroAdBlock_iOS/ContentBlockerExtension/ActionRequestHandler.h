//
//  ActionRequestHandler.h
//  MetroAdBlock
//
//  Created by Denis on 9/27/15.
//  Copyright © 2015 OpenAdblock. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ActionRequestHandler : NSObject <NSExtensionRequestHandling>

@end
